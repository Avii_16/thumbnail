// npm dependencies
import React from 'react';

// css imports
import './thumbnail.css';

/**
 * FUNCTIONAL COMPONENT : This component holds the attributes of input tag.
 * @param {object}    props    It carries the details related to the input tag.
 */
export const InputBox = (props) => (
  <input
    autoComplete='off'
    className="form-control"
    name={props.name}
    disabled={props.disabled}
    type={props.inputType}
    value={props.value}
    onChange={props.onChange}
    placeholder={props.placeholder}
  />
);

/**
 * FUNCTIONAL COMPONENT : This component holds displaying image and its properties.
 * @param {object}    props    It carries image path and alternative name of image.
 */
export const Imagefield = (props) => <img className='card-img' src={props.image} alt={props.alt} />;

/**
 * FUNCTIONAL COMPONENT : This component holds the structure of displaying the title fields.
 * @param {object}    props    It carries the title name to be displayed.
 */
export const Titlefield = (props) => <h5 className='card-title'>{props.title}</h5>;

/**
 * FUNCTIONAL COMPONENT : This component holds the structure of displaying the text fields.
 * @param {object}    props    It carries the text name to be displayed.
 */
export const Textfield = (props) => <p className='card-text'>{props.text}</p>;