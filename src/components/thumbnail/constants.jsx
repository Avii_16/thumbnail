/**
 * CONSTANT: This is used as a constant for add text.
 */
export const ADD_TEXT = "Add";
/**
 * CONSTANT: This is used as a constant for alert message.
 */
export const ALERT_TEXT = "Content can't be empty";
/**
 * CONSTANT: This is used as a constant for alert message.
 */
export const ALT_TEXT = "Virat Kohli";