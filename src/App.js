// npm dependencies
import React from 'react';

// css imports
import './App.css';

// component imports
import Thumbnail from './components/thumbnail/thumbnail';

/**
 * FUNCTIONAL COMPONENT: It is the base component that renders first after the project gets started.
 */
const App = () => {
  return (
    <React.Fragment>
      <Thumbnail />
    </React.Fragment>
  );
};

export default App;
